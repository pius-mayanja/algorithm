#include <stdio.h>
#include <stdlib.h>


struct Node
{
    int number;
    struct Node *next;
};

// Function prototypes
struct Node *createNode(int num);
void printList(struct Node *head);
void append(struct Node **head, int num);
void prepend(struct Node **head, int num);
void deleteByKey(struct Node **head, int key);
void deleteByValue(struct Node **head, int value);
void insertAfterKey(struct Node **head, int key, int value);
void insertAfterValue(struct Node **head, int searchValue, int newValue);

int main()
{
    struct Node *head = NULL;
    int choice, data;
   


    while (1)
    {
        printf("Linked Lists\n");
        printf("1. Print List\n");
        printf("2. Append\n");
        printf("3. Prepend\n");
        printf("4. Delete\n");
        printf("5. Exit\n");
        printf("Enter your choice: ");
        scanf("%d", &choice);

switch (choice)
                {
                case 1:
                        printList(head);
                        break;
                case 2:
                        printf("Enter the value to append: ");
                        scanf("%d", &data);
                        append(&head, data);
                        break;
                case 3:
                        printf("Enter a value to prepend: ");
                        scanf("%d", &data); 
                        prepend(&head, data);
                        break;
                case 4:
                        printf("Enter the value to delete: ");
                        scanf("%d", &data);
                        deleteByValue(&head, data);
                        break;
                case 5:
                        return 1;
//If the user enters a value other than 1, 2, 3, 4, or 5, the program prints "Invalid input."                   
                default:
                        printf("Invalid input.");
                        break;
                }
        }        

    return 0;
}



// Function to create a new node with given data
struct Node* createNode(int number) {
    struct Node* newNode = (struct Node*)malloc(sizeof(struct Node));
    if (newNode == NULL) {
        printf("Error: Unable to allocate memory for a new node\n");
        exit(1);
    }

    newNode->number = number;
    newNode->next = NULL;
    return newNode;
}


// Function to add a node at the beginning of the list
void prepend(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    newNode->next = *head;
    *head = newNode;
}

// Function to add a node at the end of the list
void append(struct Node **head, int num) {
    struct Node *newNode = createNode(num);
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    struct Node *current = *head;
    while (current->next != NULL) {
        current = current->next;
    }
    current->next = newNode;
}

// Function to print all nodes in the list
void printList(struct Node *head) {
    struct Node *current = head;
    printf("[");
    while (current != NULL) {
        printf("%d", current->number);
        current = current->next;

        if(current != NULL){
            printf(", ");
        }
    }
    printf("]\n");
}

// Function to delete a Node by a Key(Position)
void deleteByKey(struct Node **head, int key){
    struct Node* current= *head;
    struct Node* prev= *head;

    if(*head == NULL){
        printf("Empty List");
    }else if (key == 1) {
        *head = current->next;
        free(current);
    } else{
        while(key != 1){
            prev = current;
            current = current->next;
            key--;
        }
        prev->next = current->next;
        free(current);
    }     
    
}

void deleteByValue(struct Node **head, int value){ 
    struct Node *current = *head;   
    struct Node *prev;    

    // to delete the first node
    if(*head == NULL){
        printf("Empty list");
    }
    else if(current->number == value){
        *head = current->next;
        free(current);        
    }
    // to delete the other nodes
    else{
        while (current->number != value){
            prev = current;
            current = current->next;
        } 
        prev->next = current->next;
        free(current);
    }    

    
}




void insertAfterKey(struct Node **head, int key, int value)
{
    struct Node *current = *head;
    
    // Traverse the list to find the node with the specified key
    while (current != NULL && current->number != key)
    {
        current = current->next;
    }
    
    
    if (current == NULL)
    {
        return;
    }
    
   
    struct Node *newNode = createNode(value);
    
   
    newNode->next = current->next;
    current->next = newNode; 
}




// Function that inserts anode after a given Value
void insertAfterValue(struct Node **head, int searchValue, int newValue){
    struct Node *newNode = createNode(newValue);
    struct Node *current = *head;    

    while (current->number != searchValue){
        current = current->next;        
    }
    newNode->next =current->next;
    current->next = newNode;  

}